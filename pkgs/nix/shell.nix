{ pkgs ? import <nixpkgs> { } }:

let

  packages = pkgs.callPackage ./default.nix { };
  
in

pkgs.mkShell {
  name = "codeguesser-shell-repo";
  packages = [
    packages.codeGuesserRepo
  ];
}

