# nix-build  --expr 'with import <nixpkgs> {}; callPackage ./default.nix {}'
{ pkgs ? import <nixpkgs> { } }:

let

  corePackages = pkgs.callPackage ./../../lib/code_guesser.sh/pkgs/nix/default.nix { };

  codeGuesserRepoSubmitScrFile = builtins.readFile ./../../src/code_guesser_repo_submit_guess.sh;
  codeGuesserRepoSubmit = pkgs.writeShellApplication {
    name = "code_guesser_repo_submit_guess.sh";
    runtimeInputs = [
      pkgs.gnused
      pkgs.coreutils-full
      pkgs.bc
      pkgs.diffutils
      pkgs.gnugrep
    ];

    text = ''
      ${codeGuesserRepoSubmitScrFile}
    '';
  };

  codeGuesserRepoCreateScrFile = builtins.readFile ./../../src/code_guesser_repo_create.sh;
  codeGuesserRepoCreate = pkgs.writeShellApplication {
    name = "code_guesser_repo_create.sh";
    runtimeInputs = [
      corePackages.codeGuesser
      pkgs.findutils
      pkgs.gnugrep
      pkgs.coreutils-full
    ];

    text = ''
      CODE_GUESSER_SH="${corePackages.codeGuesser}/bin/code_guesser.sh"
      SUBMIT_GUESS_SH="${codeGuesserRepoSubmit}/bin/code_guesser_repo_submit_guess.sh"
      ${codeGuesserRepoCreateScrFile}
    '';
  };

  codeGuesserRepoPlayScrFile = builtins.readFile ./../../src/code_guesser_repo_play.sh;
  codeGuesserRepoPlay = pkgs.writeShellApplication {
    name = "code_guesser_repo_play.sh";
    runtimeInputs = [
      pkgs.coreutils-full
      pkgs.findutils
      pkgs.gnugrep
    ];

    text = ''
      ${codeGuesserRepoPlayScrFile}
    '';
  };

  codeGuesserRepo = pkgs.symlinkJoin {
    name = "codeGuesserRepo";
    paths = [
      codeGuesserRepoCreate
      codeGuesserRepoSubmit
      codeGuesserRepoPlay
    ];
    postBuild = "echo links added";
  };

in

{

  codeGuesserRepo = codeGuesserRepo;

  meta = {
    description = "A little game to test, how good did you know your code.";
    homepage = https://gitlab.com/codeguesser/code_guesser.sh;
    maintainers = "stubbfel";
    license = pkgs.lib.licenses.mit;
    platforms = pkgs.lib.platforms.unix;
  };
}
