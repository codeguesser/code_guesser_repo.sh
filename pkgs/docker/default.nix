# nix-build  --expr 'with import <nixpkgs> {}; callPackage ./default.nix {}' && find -type l -name "result*" -exec docker load --input "{}" \;
{ pkgs ? import <nixpkgs> { } }:

let

  src = pkgs.callPackage ../nix { };

  baseImage = pkgs.dockerTools.buildLayeredImage {
    name = "code_guesser_repo.sh";
    tag = "latest";
    contents = [
      src.codeGuesserRepo
    ];
  };

  shellImage = pkgs.dockerTools.buildLayeredImage {
    name = "code_guesser_repo.sh.shell";
    tag = "latest";
    fromImage = baseImage;
    contents = [
      pkgs.coreutils-full
    ];
    config.Entrypoint = [ "${pkgs.bashInteractive}/bin/bash" ];
  };

  originOpenvscodeImage = pkgs.dockerTools.pullImage {
    imageName = "gitpod/openvscode-server";
    imageDigest = "sha256:924f877b4a861aded196e031a5204917427bf87217a554ad8b614add97362ad3";
    finalImageName = "gitpod/openvscode-server";
    finalImageTag = "latest";
    sha256 = "sha256-2ValCc4JE8+G8jRf8nklfAmMBe7bC7lBQl/ArcLetuE=";
  };

  openvscodeImage = pkgs.dockerTools.buildLayeredImage {
    name = "code_guesser_repo.sh.openvscode";
    tag = "latest";
    fromImage = originOpenvscodeImage;
    contents = [
      src.codeGuesserRepo
      pkgs.coreutils-full
    ];
    config.Entrypoint = [ "${pkgs.bashInteractive}/bin/bash" ];
  };
in

[
  baseImage
  shellImage
  openvscodeImage
]

# ${OPENVSCODE_SERVER_ROOT}/bin/openvscode-server --host 0.0.0.0 --without-connection-token --disable-telemetry --extensions-dir ${OPENVSCODE_SERVER_ROOT}/extensions --accept-server-license-terms
