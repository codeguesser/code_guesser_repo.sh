#!/bin/env bash

PKGS_VERSION="${1}"

# build images
nix-build  --expr 'with import <nixpkgs> {}; callPackage pkgs/docker/default.nix {}' && find -type l -name "result*" -exec docker load --input "{}" \;

PUBLISH_IMAGE()
{
    SRC_IMAGE="${1}"
    TARGET_IMAGE="${2}"
    docker tag "${SRC_IMAGE}:latest" "${TARGET_IMAGE}:latest"
    docker tag "${SRC_IMAGE}:latest" "${TARGET_IMAGE}:${PKGS_VERSION}"

    docker push "${TARGET_IMAGE}:latest"
    docker push "${TARGET_IMAGE}:${PKGS_VERSION}"
}

# publish base image
PUBLISH_IMAGE code_guesser_repo.sh stubbfel/code_guesser_repo.sh
PUBLISH_IMAGE code_guesser_repo.sh registry.gitlab.com/codeguesser/code_guesser_repo.sh

# publish shell image
PUBLISH_IMAGE code_guesser_repo.sh.shell stubbfel/code_guesser_repo.sh.shell
PUBLISH_IMAGE code_guesser_repo.sh.shell registry.gitlab.com/codeguesser/code_guesser_repo.sh/code_guesser_repo.sh.shell

# publish open vscode image
PUBLISH_IMAGE code_guesser_repo.sh.openvscode stubbfel/code_guesser_repo.sh.openvscode
PUBLISH_IMAGE code_guesser_repo.sh.openvscode registry.gitlab.com/codeguesser/code_guesser_repo.sh/code_guesser_repo.sh.openvscode

