#!/bin/env bash

# shellcheck disable=SC2034
USAGE_MESSAGE="Usage: $0 [OPTION]...

OPTION:
    --repo-folder \"<TARGET_REPO_PATH>\" - Path to the code guesser repo.
                                        If this option is not given, then  following path \"${PWD}/.code_guesser\"

    --with-code \"<VS_CODE_CMD>\" - Pass the command of your visaul studio code 
                                    and then the this script will be open the play in new visual studio code instance

    ** all five level can be played by default, except one of following options is used
    --level1 - play level 1 files
    --level2 - play level 2 files
    --level3 - play level 3 files
    --level4 - play level 4 files
    --level5 - play level 5 files
"

REPO_FOLDER="${PWD}/.code_guesser"
CURRENT_LEVEL=""

while [[ "$#" -gt 0 ]] ; do
    case "$1" in
        --repo-folder)
            REPO_FOLDER="${2}"
            shift 
        ;;
        --level1)
            CURRENT_LEVEL="${CURRENT_LEVEL}1"
        ;;
        --level2)
            CURRENT_LEVEL="${CURRENT_LEVEL}2"
        ;;
        --level3)
            CURRENT_LEVEL="${CURRENT_LEVEL}3"
        ;;
        --level4)
            CURRENT_LEVEL="${CURRENT_LEVEL}4"
        ;;
        --level5)
            CURRENT_LEVEL="${CURRENT_LEVEL}5"
        ;;
        --with-code)
            CODE_CMD="${2}"
            shift 
        ;;
        *)
            echo "${USAGE_MESSAGE}"
            exit 1
        ;;
    esac
    
    shift
done

if [ -z "$CURRENT_LEVEL" ]; then
    CURRENT_LEVEL="12345"
fi
 
set +e
TASK_FILE=$(find "${REPO_FOLDER}" -type f | grep "${REPO_FOLDER}/level[${CURRENT_LEVEL}]/"| sort -R | head -n 1)
echo "${TASK_FILE}"

mkdir -p "${REPO_FOLDER}/current_task"
ln -s -f "${TASK_FILE}" "${REPO_FOLDER}/current_task/current_task"

LEVEL_NAME=$(realpath "${TASK_FILE}" --relative-base="${REPO_FOLDER}" | cut -d "/" -f1)
WORK_SPACE_FILE="${REPO_FOLDER}/${LEVEL_NAME}.code-workspace"
echo "${WORK_SPACE_FILE}"
ln -s -f "${WORK_SPACE_FILE}" "${REPO_FOLDER}/current_workspace"
set -e

if [ -v CODE_CMD ]; then
    $CODE_CMD "${WORK_SPACE_FILE}" "${TASK_FILE}"
fi