#!/bin/env bash

USAGE_MESSAGE="Usage: $0 <REPO_PATH> [OPTION]...

OPTION:

    ** repo options 
    --target-repo-folder \"<TARGET_REPO_PATH>\" - Path to folder where the code guesser repo should be created.
                                                  If this option is not given, then the code guesser repo will be created at \"${PWD}/.code_guesser\"
    --answer-options-repo-folder - Path to the folder which should show as possible answers.
                                   If this option is not given, then the solution folder of code guesser repo will be used.
    --disable-answer-options-repo-folder - create workspace files without answer folder
    --no-workspace-files - create no workspace files
    --no-file-path-obfuscation - disable file content obfuscation, only the file and folder names will be obfuscated.          

    ** stop word lists (if no one is given, a stop word list based on file extension will be used)
    --no-defaults - disable default stop word list
    --cs - use default stop word list for csharp
    --c - use default stop word list for c
    --cpp - use default stop word list for cpp
    --js - use default stop word list for javascript
    --java - use default stop word list for java
    --rs - use default stop word list for rust
    --xml - use default stop word list for xml
    --all - use all default stop word lists

    ** all five level will be created by default, except one of following options is used
    --level1 - create files level 1
    --level2 - create files level 2
    --level3 - create files level 3
    --level4 - create files level 4
    --level5 - create files level 5
"

if [ $# -lt 1 ]; then
    echo "${USAGE_MESSAGE}"
    exit 1
fi

SCRIPT_FOLDER=$(realpath "$0")
SCRIPT_FOLDER=$(dirname "${SCRIPT_FOLDER}")

CODE_GUESSER_SH="${CODE_GUESSER_SH:=$(realpath "${SCRIPT_FOLDER}"/../lib/code_guesser.sh/src/code_guesser.sh)}"
SUBMIT_GUESS_SH="${SUBMIT_GUESS_SH:=$(realpath "${SCRIPT_FOLDER}/code_guesser_repo_submit_guess.sh")}"

SOURCE_FOLDER="$1"
shift


LEVEL_ARRAY=()

FILE_ARGS=(
    "--non-interactive"
    "--quiet"
    "--skip-solution"
)

CALC_CLEAR_BASE_FILE_PATH()
{
    CLEAR_BASE_FILE_PATH=$(realpath "${SOURCE_FILE}" --relative-base="${SOURCE_FOLDER}")
}

SETUP_OBFUSCATION_BASE_FILE_PATH_PARTS()
{
    CALC_CLEAR_BASE_FILE_PATH

    readarray -td/ CLEAR_BASE_FILE_PATH_PARTS <<< "${CLEAR_BASE_FILE_PATH}";

    OBFUSCATION_BASE_FILE_PATH_PARTS=()
}

UPDATE_OBFUSCATION_BASE_FILE_PATH()
{
   OBFUSCATION_BASE_FILE_PATH_PARTS+=("${1}")
}

CALC_STRING_SIZE_OR_MAX()
{
    INPUT_STRING="${1//[[:space:]]/}"
    INPUT_SIZE=${#INPUT_STRING}
    
    if [ "${INPUT_SIZE}" -gt "${2}" ]; then
        STRING_SIZE_OR_MAX=64
    else
        STRING_SIZE_OR_MAX="${INPUT_SIZE}"
    fi
}

CALC_CODE_GUESSER_HASH()
{
    CODE_GUESSER_HASH=$(echo "${1}" | sha256sum | head -c "${2}")
}

CALC_PATH_PART_HASH()
{
    CALC_CODE_GUESSER_HASH "$1" "$2"
    PATH_PART_HASH="${CODE_GUESSER_HASH}"
}

CALC_CODE_GUESSER_HASH_BASE_FILE_NAME()
{
    CALC_STRING_SIZE_OR_MAX "${CLEAR_BASE_FILE_NAME}" 64
    HASH_BASE_FILE_NAME=$(echo "${1}" | sha256sum | head -c "${STRING_SIZE_OR_MAX}")
}

OBFUSCATION_BASE_FILE_PATH_PARTS_TO_BASE_FILE_PATH()
{
    BASE_FILE_PATH=$(IFS=/ ; echo "${OBFUSCATION_BASE_FILE_PATH_PARTS[*]}")
}

CALC_CLEAR_FILE_NAME_AND_EXTENSION()
{
    LAST_CLEAR_BASE_FILE_PATH_PART=$1
    CLEAR_BASE_FILE_NAME="${LAST_CLEAR_BASE_FILE_PATH_PART%.*}"
    CLEAR_BASE_FILE_EXTENSION="${LAST_CLEAR_BASE_FILE_PATH_PART#"${CLEAR_BASE_FILE_NAME}"}"
    CLEAR_BASE_FILE_EXTENSION="${CLEAR_BASE_FILE_EXTENSION##*.}"
    CLEAR_BASE_FILE_EXTENSION="${CLEAR_BASE_FILE_EXTENSION//[[:space:]]/}"
}

CALC_PATH_PART_HASH_WITH_CLEAR_UPPER_CHAR()
{
    HASH_PARTS=$1
    CLEAR_PARTS=$2
    for (( i=0; i<${#HASH_PARTS}; i++ )); do
        CLEAR_CHAR="${CLEAR_PARTS:$i:1}"
        HASH_CHAR="${HASH_PARTS:$i:1}"

        if [[ "$CLEAR_CHAR" =~ [A-Z] ]]
        then
            PATH_PART_HASH="${PATH_PART_HASH}${CLEAR_CHAR}"
        else
            PATH_PART_HASH="${PATH_PART_HASH}${HASH_CHAR}"
        fi
    done
}

FILE_PATH_OBFUSCATION_LEVEL1()
{   
    SETUP_OBFUSCATION_BASE_FILE_PATH_PARTS

    for CLEAR_BASE_FILE_PATH_PART in "${CLEAR_BASE_FILE_PATH_PARTS[@]}"; do
        CALC_PATH_PART_HASH "${CLEAR_BASE_FILE_PATH_PART}" 64
        UPDATE_OBFUSCATION_BASE_FILE_PATH "${PATH_PART_HASH}"
    done   
    
    OBFUSCATION_BASE_FILE_PATH_PARTS_TO_BASE_FILE_PATH
}

FILE_PATH_OBFUSCATION_LEVEL2()
{
    SETUP_OBFUSCATION_BASE_FILE_PATH_PARTS

    for CLEAR_BASE_FILE_PATH_PART in "${CLEAR_BASE_FILE_PATH_PARTS[@]}"; do
        CALC_STRING_SIZE_OR_MAX "${CLEAR_BASE_FILE_PATH_PART}" 64
        CALC_PATH_PART_HASH "${CLEAR_BASE_FILE_PATH_PART}" "${STRING_SIZE_OR_MAX}"
        UPDATE_OBFUSCATION_BASE_FILE_PATH "${PATH_PART_HASH}"
    done   
    
    OBFUSCATION_BASE_FILE_PATH_PARTS_TO_BASE_FILE_PATH
}

FILE_PATH_OBFUSCATION_LEVEL3()
{
    SETUP_OBFUSCATION_BASE_FILE_PATH_PARTS

    for CLEAR_BASE_FILE_PATH_PART in "${CLEAR_BASE_FILE_PATH_PARTS[@]::${#CLEAR_BASE_FILE_PATH_PARTS[@]}-1}"; do
        CALC_STRING_SIZE_OR_MAX "${CLEAR_BASE_FILE_PATH_PART}" 64
        CALC_PATH_PART_HASH "${CLEAR_BASE_FILE_PATH_PART}" "${STRING_SIZE_OR_MAX}"
        UPDATE_OBFUSCATION_BASE_FILE_PATH "${PATH_PART_HASH}"
    done   

    CALC_CLEAR_FILE_NAME_AND_EXTENSION "${CLEAR_BASE_FILE_PATH_PARTS[-1]}"   

    CALC_CODE_GUESSER_HASH_BASE_FILE_NAME "${CLEAR_BASE_FILE_NAME}"

    CALC_STRING_SIZE_OR_MAX "${CLEAR_BASE_FILE_EXTENSION}" 64
    CALC_CODE_GUESSER_HASH "${CLEAR_BASE_FILE_EXTENSION}" "${STRING_SIZE_OR_MAX}"
    HASH_BASE_FILE_EXTENSION="${CODE_GUESSER_HASH}"
    
    UPDATE_OBFUSCATION_BASE_FILE_PATH "${HASH_BASE_FILE_NAME}.${HASH_BASE_FILE_EXTENSION}"
    
    OBFUSCATION_BASE_FILE_PATH_PARTS_TO_BASE_FILE_PATH
}

FILE_PATH_OBFUSCATION_LEVEL4()
{
    SETUP_OBFUSCATION_BASE_FILE_PATH_PARTS

    for CLEAR_BASE_FILE_PATH_PART in "${CLEAR_BASE_FILE_PATH_PARTS[@]::${#CLEAR_BASE_FILE_PATH_PARTS[@]}-1}"; do
        CALC_STRING_SIZE_OR_MAX "${CLEAR_BASE_FILE_PATH_PART}" 64
        CALC_PATH_PART_HASH "${CLEAR_BASE_FILE_PATH_PART}" "${STRING_SIZE_OR_MAX}"
        UPDATE_OBFUSCATION_BASE_FILE_PATH "${PATH_PART_HASH}"
    done   

    CALC_CLEAR_FILE_NAME_AND_EXTENSION "${CLEAR_BASE_FILE_PATH_PARTS[-1]}"

    CALC_CODE_GUESSER_HASH_BASE_FILE_NAME "${CLEAR_BASE_FILE_NAME}"
    
    UPDATE_OBFUSCATION_BASE_FILE_PATH "${HASH_BASE_FILE_NAME}.${CLEAR_BASE_FILE_EXTENSION}"
    
    OBFUSCATION_BASE_FILE_PATH_PARTS_TO_BASE_FILE_PATH
}

FILE_PATH_OBFUSCATION_LEVEL5()
{
    SETUP_OBFUSCATION_BASE_FILE_PATH_PARTS

    for CLEAR_BASE_FILE_PATH_PART in "${CLEAR_BASE_FILE_PATH_PARTS[@]::${#CLEAR_BASE_FILE_PATH_PARTS[@]}-1}"; do
        CALC_STRING_SIZE_OR_MAX "${CLEAR_BASE_FILE_PATH_PART}" 64
        CALC_PATH_PART_HASH "${CLEAR_BASE_FILE_PATH_PART}" "${STRING_SIZE_OR_MAX}"
        PATH_PART_HASH_TMP="${PATH_PART_HASH}"
        
        PATH_PART_HASH=""
        CALC_PATH_PART_HASH_WITH_CLEAR_UPPER_CHAR "$PATH_PART_HASH_TMP" "$CLEAR_BASE_FILE_PATH_PART"

        UPDATE_OBFUSCATION_BASE_FILE_PATH "${PATH_PART_HASH}"
    done   

    CALC_CLEAR_FILE_NAME_AND_EXTENSION "${CLEAR_BASE_FILE_PATH_PARTS[-1]}"

    CALC_CODE_GUESSER_HASH_BASE_FILE_NAME "${CLEAR_BASE_FILE_NAME}"   

    PATH_PART_HASH=""
    CALC_PATH_PART_HASH_WITH_CLEAR_UPPER_CHAR "$HASH_BASE_FILE_NAME" "$CLEAR_BASE_FILE_NAME"
    HASH_BASE_FILE_NAME="${PATH_PART_HASH}"
    
    UPDATE_OBFUSCATION_BASE_FILE_PATH "${HASH_BASE_FILE_NAME}.${CLEAR_BASE_FILE_EXTENSION}"
    
    OBFUSCATION_BASE_FILE_PATH_PARTS_TO_BASE_FILE_PATH
}

NO_FILE_PATH_OBFUSCATION()
{
    CALC_CLEAR_BASE_FILE_PATH
    BASE_FILE_PATH="${CLEAR_BASE_FILE_PATH}"
}

FILE_PATH_OBFUSCATION_LEVEL1_CMD=(
    'FILE_PATH_OBFUSCATION_LEVEL1'
)

FILE_PATH_OBFUSCATION_LEVEL2_CMD=(
    'FILE_PATH_OBFUSCATION_LEVEL2'
)

FILE_PATH_OBFUSCATION_LEVEL3_CMD=(
    'FILE_PATH_OBFUSCATION_LEVEL3'
)

FILE_PATH_OBFUSCATION_LEVEL4_CMD=(
    'FILE_PATH_OBFUSCATION_LEVEL4'
)

FILE_PATH_OBFUSCATION_LEVEL5_CMD=(
    'FILE_PATH_OBFUSCATION_LEVEL5'
)

INIT_REPO_FOLDER()
{
    REPO_FOLDER="${1}"
    SOLUTION_FOLDER="${REPO_FOLDER}/solution"
    ANSWER_OPTIONS_REPO_FOLDER="${SOLUTION_FOLDER}"
    ANSWER_OPTIONS_REPO_WORK_SPACE_FOLDER="${REPO_FOLDER}/answer_options"
}

INIT_REPO_FOLDER "${PWD}/.code_guesser"

while [[ "$#" -gt 0 ]] ; do
    case "$1" in
        --target-repo-folder)
            INIT_REPO_FOLDER "$(realpath "${2}")"
            shift 
        ;;
        --answer-options-repo-folder)
            ANSWER_OPTIONS_REPO_FOLDER=$(realpath "${2}")
            shift 
        ;;
        --disable-answer-options-repo-folder)            
            unset ANSWER_OPTIONS_REPO_WORK_SPACE_FOLDER
        ;;
        --level1)
            LEVEL_ARRAY+=("1")
        ;;
        --level2)
            LEVEL_ARRAY+=("2")
        ;;
        --level3)
            LEVEL_ARRAY+=("3")
        ;;
        --level4)
            LEVEL_ARRAY+=("4")
        ;;
        --level5)
            LEVEL_ARRAY+=("5")
        ;;
        --no-workspace-files)
            NO_WORKSPACE_FILES="y"
        ;;
        --no-file-path-obfuscation)
            # shellcheck disable=SC2034
            FILE_PATH_OBFUSCATION_LEVEL1_CMD=(
                'NO_FILE_PATH_OBFUSCATION'
            )

            # shellcheck disable=SC2034
            FILE_PATH_OBFUSCATION_LEVEL2_CMD=(
                'NO_FILE_PATH_OBFUSCATION'
            )

            # shellcheck disable=SC2034
            FILE_PATH_OBFUSCATION_LEVEL3_CMD=(
                'NO_FILE_PATH_OBFUSCATION'
            )

            # shellcheck disable=SC2034
            FILE_PATH_OBFUSCATION_LEVEL4_CMD=(
                'NO_FILE_PATH_OBFUSCATION'
            )

            # shellcheck disable=SC2034
            FILE_PATH_OBFUSCATION_LEVEL5_CMD=(
                'NO_FILE_PATH_OBFUSCATION'
            )
        ;;
        *)
            FILE_ARGS+=("$1")
        ;;
    esac
    
    shift
done

if [ ${#LEVEL_ARRAY[@]} -eq 0 ]; then
   LEVEL_ARRAY=(
        "1"
        "2"
        "3"
        "4"
        "5"
    )
fi

set +e
mapfile -t SOURCE_FILES < <(find "${SOURCE_FOLDER}" -type f -exec grep -Il . {} +)

CREATE_SOLUTION_FILE()
{    
    CALC_CLEAR_BASE_FILE_PATH   
    TARGET_FILE="${SOLUTION_FOLDER}/${CLEAR_BASE_FILE_PATH}"
    TARGET_FOLDER=$(dirname "${TARGET_FILE}")

    mkdir -p "${TARGET_FOLDER}"
    cp -f "${SOURCE_FILE}" "${TARGET_FILE}"
}

for SOURCE_FILE in "${SOURCE_FILES[@]}"; do
    CREATE_SOLUTION_FILE 
done

CREATE_LEVEL_FILE()
{    
    FILE_PATH_OBFUSCATIO_LEVEL_CMD="FILE_PATH_OBFUSCATION_LEVEL${CURRENT_LEVEL}_CMD"
    eval "eval \"\${${FILE_PATH_OBFUSCATIO_LEVEL_CMD}[@]}\""  
    
    BASE_FOLDER_PATH=$(dirname "${BASE_FILE_PATH}")

    LEVEL_FOLDER="${REPO_FOLDER}/level${CURRENT_LEVEL}"

    LINK_TO_SOLUTION_FOLDER="${REPO_FOLDER}/to_solution_map/level${CURRENT_LEVEL}/${BASE_FOLDER_PATH}"
    LINK_TO_SOLUTION_FILE="${REPO_FOLDER}/to_solution_map/level${CURRENT_LEVEL}/${BASE_FILE_PATH}"

    TARGET_FILE="${LEVEL_FOLDER}/${BASE_FILE_PATH}"
    TARGET_FOLDER="${LEVEL_FOLDER}/${BASE_FOLDER_PATH}"

    mkdir -p "${TARGET_FOLDER}"
    "${CODE_GUESSER_SH}" "${SOURCE_FILE}" "${FILE_ARGS[@]}" "--level${CURRENT_LEVEL}" > "${TARGET_FILE}"

    mkdir -p "${LINK_TO_SOLUTION_FOLDER}"
    RELATIV_LINK_TARGET=$(realpath "${REPO_FOLDER}/solution/${CLEAR_BASE_FILE_PATH}" --relative-to="${TARGET_FILE}")
    ln -s -f "${RELATIV_LINK_TARGET}" "${LINK_TO_SOLUTION_FILE}"
}

for SOURCE_FILE in "${SOURCE_FILES[@]}"; do
    for CURRENT_LEVEL in "${LEVEL_ARRAY[@]}"; do
        CREATE_LEVEL_FILE
    done    
done

if [ -v NO_WORKSPACE_FILES ]; then
    set -e
    exit 0
fi


WRITE_WORK_SPACE_FILE()
{   
    WORK_FOLDER_PATHS=""
    for WORK_FOLDER_PATH in "$@"; do    
         WORK_FOLDER_PATHS="${WORK_FOLDER_PATHS}{\"path\": \"${WORK_FOLDER_PATH}\"},"      
    done

    WORK_SPACE_FILE_CONTENT="{
        \"folders\": [
            $WORK_FOLDER_PATHS
            {\"path\": \"current_task\"},
        ]
    }"

    WORK_SPACE_BASE_FILE_NAME=$(basename "${1}")
    echo "${WORK_SPACE_FILE_CONTENT}" > "${REPO_FOLDER}/${WORK_SPACE_BASE_FILE_NAME}.code-workspace"
}

WRITE_WORK_SPACE_FILE "solution"

if [ -v ANSWER_OPTIONS_REPO_WORK_SPACE_FOLDER ]; then
    rm -f "$ANSWER_OPTIONS_REPO_WORK_SPACE_FOLDER"
    ANSWER_OPTIONS_REPO_FOLDER=$(realpath "${ANSWER_OPTIONS_REPO_FOLDER}" --relative-to="${REPO_FOLDER}")
    ln -s -f "${ANSWER_OPTIONS_REPO_FOLDER}" "${ANSWER_OPTIONS_REPO_WORK_SPACE_FOLDER}"
fi

for CURRENT_LEVEL in "${LEVEL_ARRAY[@]}"; do    
    WRITE_WORK_SPACE_FILE "level${CURRENT_LEVEL}" "$ANSWER_OPTIONS_REPO_WORK_SPACE_FOLDER"
done

echo "#!/bin/env bash
${SUBMIT_GUESS_SH} \"${REPO_FOLDER}/current_task/current_task\" \"\$@\" --repo-folder \"${REPO_FOLDER}\"" > "${REPO_FOLDER}/submit_guess.sh"
chmod u+x "${REPO_FOLDER}/submit_guess.sh"

set -e
