#!/bin/env bash

USAGE_MESSAGE="Usage: $0 \"<TASK_FILE_PATH>\" \"<GUESS_FILE_PATH>\" [OPTION]...

OPTION:
    --repo-folder \"<TARGET_REPO_PATH>\" - Path to the code guesser repo.
                                           If this option is not given, then  following path \"${PWD}/.code_guesser\"
    --max-points <MAX_POINTS> - the max points, of the guess is correct.
    --skip-solution - did not show the solution
"

if [ $# -lt 1 ]; then
    echo "${USAGE_MESSAGE}"
    exit 1
fi

TASK_FILE=$(realpath "${1}")
shift

GUESS_FILE=$(realpath "${1}")
shift

REPO_FOLDER="${PWD}/.code_guesser"
MAX_POINTS=5000

while [[ "$#" -gt 0 ]] ; do
    case "$1" in
        --repo-folder)
            REPO_FOLDER="${2}"
            shift 
        ;;
        --max-points)
            MAX_POINTS="${2}" 
            shift 
        ;;
        --skip-solution)
            SKIP_SOLUTION="y"
        ;;
        *)
        TASK_FILE=$(realpath "${1}")
        ;;
    esac
    
    shift
done

set +e 
TASK_BASE_FILE_PATH=$(realpath "${TASK_FILE}" --relative-base="${REPO_FOLDER}")
TASK_SOLUTION_FILE="${REPO_FOLDER}/to_solution_map/${TASK_BASE_FILE_PATH}";

CALC_CHAR_COUNT()
{
    CHAR_COUNT=$(wc -m < "${1}")
}

CALC_CHAR_COUNT "${TASK_SOLUTION_FILE}"
TASK_SOLUTION_FILE_CHAR_COUNT="${CHAR_COUNT}"

CALC_CHAR_COUNT "${GUESS_FILE}"
GUESS_FILE_CHAR_COUNT="${CHAR_COUNT}"

((TOTAT_CHAR_COUNT=TASK_SOLUTION_FILE_CHAR_COUNT + GUESS_FILE_CHAR_COUNT))

CALC_CHAR_DIFF_FILE()
{
    # mv every char into a own line
    CHAR_DIFF_FILE=$(sed -E "s/./&\n/g" < "${1}")
}

CALC_CHAR_DIFF_FILE "${TASK_SOLUTION_FILE}"
TASK_SOLUTION_DIFF_FILE="${CHAR_DIFF_FILE}"

CALC_CHAR_DIFF_FILE "${GUESS_FILE}"
GUESS_DIFF_FILE="${CHAR_DIFF_FILE}"

DIFF_CHAR_COUNT=$(diff <(echo "${TASK_SOLUTION_DIFF_FILE}") <(echo "${GUESS_DIFF_FILE}") --suppress-common-lines | grep -e "^>" -e "^<" -c)
DIFF_CHAR_COUNT=$(echo "${DIFF_CHAR_COUNT} / 2" | bc -l)
 
POINTS=$(echo "((${TOTAT_CHAR_COUNT} - ${DIFF_CHAR_COUNT}) / ${TOTAT_CHAR_COUNT}) * ${MAX_POINTS}" | bc -l | sed 's/^\./0./')
POINTS="${POINTS%%.*}"

echo "${POINTS}"

if [ -v SKIP_SOLUTION ]; then
    set -e
    exit 0
fi

realpath "${TASK_SOLUTION_FILE}"
set -e