# code_guesser_repo.sh

A little game to test how good did you know your code.

These scripts obfuscation given repository:

1. the file content like [`code_guesser.sh`](https://gitlab.com/codeguesser/code_guesser.sh)
2. the file path:
    * level 1 -> `8a8f60ecb09b7e64c6d5214a8043865e608507db8c3f61f995eae6d078875901/0293154c6c178655209c3d567f2e2a7a1c57c7b7fc3ee3639af3fa6b61bcfc3f`
    * level 2 -> `8a8/0293154c6c`
    * level 3 -> `8a8/1355035.30`
    * level 4 -> `8a8/1355035.cs`
    * level 5 -> `8a8/E1A3063.cs`


## Usage

1. create are code guesser repo

```
Usage: code_guesser_repo_create.sh <REPO_PATH> [OPTION]...

OPTION:

    ** repo options 
    --target-repo-folder \"<TARGET_REPO_PATH>\" - Path to folder where the code guesser repo should be created.
                                                  If this option is not given, then the code guesser repo will be created at \"${PWD}/.code_guesser\"
    --answer-options-repo-folder - Path to the folder which should show as possible answers.
                                   If this option is not given, then the solution folder of code guesser repo will be used.
    --disable-answer-options-repo-folder - create workspace files without answer folder
    --no-workspace-files - create no workspace files
    --no-file-path-obfuscation - disable file content obfuscation, only the file and folder names will be obfuscated.          

    ** stop word lists (if no one is given, a stop word list based on file extension will be used)
    --no-defaults - disable default stop word list
    --cs - use default stop word list for csharp
    --c - use default stop word list for c
    --cpp - use default stop word list for cpp
    --js - use default stop word list for javascript
    --java - use default stop word list for java
    --rs - use default stop word list for rust
    --xml - use default stop word list for xml
    --all - use all default stop word lists

    ** all five level will be created by default, except one of following options is used
    --level1 - create files level 1
    --level2 - create files level 2
    --level3 - create files level 3
    --level4 - create files level 4
    --level5 - create files level 5
```

2. create a play

```
Usage: code_guesser_repo_play.sh [OPTION]...

OPTION:
    --repo-folder \"<TARGET_REPO_PATH>\" - Path to the code guesser repo.
                                        If this option is not given, then  following path \"${PWD}/.code_guesser\"

    --with-code \"<VS_CODE_CMD>\" - Pass the command of your visaul studio code 
                                    and then the this script will be open the play in new visual studio code instance

    ** all five level can be played by default, except one of following options is used
    --level1 - play level 1 files
    --level2 - play level 2 files
    --level3 - play level 3 files
    --level4 - play level 4 files
    --level5 - play level 5 files
```

3. submit a guess

```
Usage: code_guesser_repo_submit_guess.sh \"<TASK_FILE_PATH>\" \"<GUESS_FILE_PATH>\" [OPTION]...

OPTION:
    --repo-folder \"<TARGET_REPO_PATH>\" - Path to the code guesser repo.
                                           If this option is not given, then  following path \"${PWD}/.code_guesser\"
    --max-points <MAX_POINTS> - the max points, of the guess is correct.
    --skip-solution - did not show the solution
```

## Example

```shell
# create a code guesser repo at .code_guesser
src/code_guesser_repo_create.sh example-repo

# play a round with visual studio code
src/code_guesser_repo_play.sh --with-code code

# guess a file for current task/play
src/code_guesser_repo_submit_guess.sh .code_guesser/current_task/current_task example-repo/app/example.java

# shortcut for submit a guess for current play
.code_guesser/submit_guess.sh  example-repo/app/example.java 
```

## Packages

### Nix Package

```shell
# build nix package
nix-build pkgs/nix/

# use script from package
result/bin/code_guesser_repo_create.sh example-repo

# run code guesser inside a pure nix shell
nix-shell pkgs/nix/shell.nix --pure --run "src/code_guesser_repo_play.sh"
```

### Docker

```shell
# local docker build
nix-build  --expr 'with import <nixpkgs> {}; callPackage pkgs/docker/default.nix {}' && find -type l -name "result*" -exec docker load --input "{}" \;

# run code guesser repo inside a docker container (file are mounted as volume)
docker run --rm -ti -v $PWD:/repo code_guesser_repo.sh code_guesser_repo_create.sh /repo/example-repo/ --target-repo-folder /repo/.cg
docker run --rm -ti -v $PWD:/repo code_guesser_repo.sh code_guesser_repo_play.sh --repo-folder /repo/.cg
docker run --rm -ti -v $PWD:/repo code_guesser_repo.sh code_guesser_repo_submit_guess.sh /repo/.cg/current_task/current_task /repo/example-repo/app/example.java --repo-folder /repo/.cg

# run code guesser repo in container with bash shell
docker run --rm -ti -v $PWD:/repo code_guesser_repo.sh.shell

# run code guesser with openvscode-server (only x64)
docker run --rm -ti -v $PWD:/repo -w /repo -p 3000:3000 code_guesser_repo.sh.openvscode -c "
    code_guesser_repo_create.sh example-repo \
        && code_guesser_repo_play.sh \
        && \${OPENVSCODE_SERVER_ROOT}/bin/openvscode-server --host 0.0.0.0 --without-connection-token --disable-telemetry --extensions-dir \${OPENVSCODE_SERVER_ROOT}/extensions --accept-server-license-terms
"     

# then you can open the play with browser http://localhost:3000/?workspace=/repo/.code_guesser/current_workspace
# inside the browser open current_task/current_task and try to find clear text version of this file in answer_options folder.
# Above the answer_options folder you can find the level folder and other obfuscated files
# HINT: If you open the terminal, then you can run following command, where the current task si placed inside the level folder
cd /repo && ls -lah .code_guesser/current_task/current_task

# when you want to submit a guess, then cope the (full) path of the file and run following task in a terminal of the open vs
cd /repo && .code_guesser/submit_guess.sh "<GUESS_FILE_PATH>"

# if you to play a next round, then run following command and reload the window
cd /repo && code_guesser_repo_play.sh 

# also can directly pulled from hub.docker.com
docker run --rm -ti -v $PWD:/repo stubbfel/code_guesser_repo.sh.shell
```